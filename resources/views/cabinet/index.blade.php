@extends('layouts.app')
@push('styles')
<link rel="stylesheet" href="{{ mix('css/app.css') }}">
@endpush
@section('content')
<div class="busines-news-block">
    <div class="container">
        <div class="wrapper-busines-news-block">
            <div class="title-busines-news">Кабинет</div>
            <div class="block-tag">
                <div class="main-point">Главная</div>
                <div class="span-arrow">
                    <img src="/image/icon/green-arrow-right.png" alt="">
                </div>
                <div class="main-point">Кабинет</div>
            </div>

        </div>
    </div>
</div>
<div class="item-news-block">
    <div class="container">
        <div class="wrapper-item-news-block">
            <div class="column-right-pos mg-right">
                <div class="menu-cabinet">
                    <img src="/image/pictur/menu.png" alt="">
                </div>
                <div class="banner-search">
                    <img src="/image/pictur/Banner.png" alt="">
                </div>
            </div>
            <div class="column-left-pos">
                <div class="startups-column">
                    <div class="str-item-img">
                        <img src="/image/pictur/tower-comec.png" alt="">
                    </div>
                    <div class="str-item-date">01.02.2021</div>
                    <div class="str-item-price">
                        <span>15% в год</span><span>$ 350.000 мин</span>
                    </div>
                    <div class="str-item-text">ТОО "Инвест" Продам инвестиционный контракт на сумму 12 000 000 евро
                    </div>
                    <div class="str-ite-btn">
                        <button>Контакты</button>
                    </div>
                    <div class="str-item-desc">Продажа</div>
                </div>
                <div class="startups-column">
                    <div class="str-item-img">
                        <img src="/image/pictur/tower-comec.png" alt="">
                    </div>
                    <div class="str-item-date">01.02.2021</div>
                    <div class="str-item-price">
                        <span>15% в год</span><span>$ 350.000 мин</span>
                    </div>
                    <div class="str-item-text">ТОО "Инвест" Продам инвестиционный контракт на сумму 12 000 000 евро
                    </div>
                    <div class="str-ite-btn">
                        <button>Контакты</button>
                    </div>
                    <div class="str-item-desc">Продажа</div>
                </div>
                <div class="startups-column">
                    <div class="str-item-img">
                        <img src="/image/pictur/tower-comec.png" alt="">
                    </div>
                    <div class="str-item-date">01.02.2021</div>
                    <div class="str-item-price">
                        <span>15% в год</span><span>$ 350.000 мин</span>
                    </div>
                    <div class="str-item-text">ТОО "Инвест" Продам инвестиционный контракт на сумму 12 000 000 евро
                    </div>
                    <div class="str-ite-btn">
                        <button>Контакты</button>
                    </div>
                    <div class="str-item-desc">Продажа</div>
                </div>
                <div class="startups-column">
                    <div class="str-item-img">
                        <img src="/image/pictur/tower-comec.png" alt="">
                    </div>
                    <div class="str-item-date">01.02.2021</div>
                    <div class="str-item-price">
                        <span>15% в год</span><span>$ 350.000 мин</span>
                    </div>
                    <div class="str-item-text">ТОО "Инвест" Продам инвестиционный контракт на сумму 12 000 000 евро
                    </div>
                    <div class="str-ite-btn">
                        <button>Контакты</button>
                    </div>
                    <div class="str-item-desc">Продажа</div>
                </div>
                <div class="startups-column">
                    <div class="str-item-img">
                        <img src="/image/pictur/tower-comec.png" alt="">
                    </div>
                    <div class="str-item-date">01.02.2021</div>
                    <div class="str-item-price">
                        <span>15% в год</span><span>$ 350.000 мин</span>
                    </div>
                    <div class="str-item-text">ТОО "Инвест" Продам инвестиционный контракт на сумму 12 000 000 евро
                    </div>
                    <div class="str-ite-btn">
                        <button>Контакты</button>
                    </div>
                    <div class="str-item-desc">Продажа</div>
                </div>
                <div class="startups-column">
                    <div class="str-item-img">
                        <img src="/image/pictur/tower-comec.png" alt="">
                    </div>
                    <div class="str-item-date">01.02.2021</div>
                    <div class="str-item-price">
                        <span>15% в год</span><span>$ 350.000 мин</span>
                    </div>
                    <div class="str-item-text">ТОО "Инвест" Продам инвестиционный контракт на сумму 12 000 000 евро
                    </div>
                    <div class="str-ite-btn">
                        <button>Контакты</button>
                    </div>
                    <div class="str-item-desc">Продажа</div>
                </div>
                <div class="startups-column">
                    <div class="str-item-img">
                        <img src="/image/pictur/tower-comec.png" alt="">
                    </div>
                    <div class="str-item-date">01.02.2021</div>
                    <div class="str-item-price">
                        <span>15% в год</span><span>$ 350.000 мин</span>
                    </div>
                    <div class="str-item-text">ТОО "Инвест" Продам инвестиционный контракт на сумму 12 000 000 евро
                    </div>
                    <div class="str-ite-btn">
                        <button>Контакты</button>
                    </div>
                    <div class="str-item-desc">Продажа</div>
                </div>
                <div class="startups-column">
                    <div class="str-item-img">
                        <img src="/image/pictur/tower-comec.png" alt="">
                    </div>
                    <div class="str-item-date">01.02.2021</div>
                    <div class="str-item-price">
                        <span>15% в год</span><span>$ 350.000 мин</span>
                    </div>
                    <div class="str-item-text">ТОО "Инвест" Продам инвестиционный контракт на сумму 12 000 000 евро
                    </div>
                    <div class="str-ite-btn">
                        <button>Контакты</button>
                    </div>
                    <div class="str-item-desc">Продажа</div>
                </div>
            </div>
        </div>
        <div class="pagination-block">
            <div class="item-pagionation">
                <ul>
                    <li class="active-pag">1</li>
                    <li>2</li>
                    <li>3</li>
                    <li>...</li>
                    <li><img src="/image/icon/black-arrow-right.png" alt=""></li>
                </ul>
            </div>
        </div>
        <div class="banner-join-investor">
            <div class="container">
                <div class="wrapper-banner-join-investor">
                    <div class="title-banner-join-investor">Присоединяйся к нашей профессиональной команде</div>
                    <div class="desc-banner-join-investor">ФРИЛАНС БИЗНЕС ПЛАТФОРМА ДЛЯ ИНВЕСТИЦИЙ</div>
                    <div class="btn-banner-join">
                        <button>Начать</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
