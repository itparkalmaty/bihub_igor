@extends('layouts.app')
@push('styles')
<link rel="stylesheet" href="{{ mix('css/app.css') }}">
@endpush
@section('content')
<div class="busines-news-block">
    <div class="container">
        <div class="wrapper-busines-news-block">
            <div class="title-busines-news">Карточка новости</div>
            <div class="block-tag">
                <div class="main-point">Главная</div>
                <div class="span-arrow">
                    <img src="/image/icon/green-arrow-right.png" alt="">
                </div>
                <div class="main-point">Карточка новости</div>
            </div>

        </div>
    </div>
</div>
<div class="item-news-block">
    <div class="container">
        <div class="wrapper-item-news-block">
            <div class="column-left-pos card-pos">
                <div class="main-pict-card-item">
                    <img src="/image/pictur/main-card-item.png" alt="">
                </div>
                <div class="title-card-item">При создании генератора мы использовали код речей</div>
                <div class="description-card-item">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру
                    сгенерировать несколько абзацев более менее осмысленного текста рыбы на русском языке, а начинающему
                    оратору отточить навык публичных выступлений в домашних условиях. При создании генератора мы
                    использовали небезизвестный универсальный код речей. Текст генерируется абзацами случайным образом
                    от двух до десяти предложений в абзаце, что позволяет сделать текст более привлекательным и живым
                    для визуально-слухового восприятия.</div>
                <div class="description-card-item"> По своей сути рыбатекст является альтернативой традиционному lorem
                    ipsum, который вызывает у
                    некторых людей недоумение при попытках прочитать рыбу текст. В отличии от lorem ipsum, текст рыба на
                    русском языке наполнит любой макет непонятным смыслом и придаст неповторимый колорит советских
                    времен.</div>
                <div class="subtitle-ul-item-block">Пример пронумерованного списка:</div>
                <ol class="card-ol-item">
                    <li>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более
                        менее осмысленного текста рыбы на русском языке</li>
                    <li>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более
                        менее осмысленного текста рыбы на русском языке</li>
                    <li>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более
                        менее осмысленного текста рыбы на русском языке</li>
                    <li>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более
                        менее осмысленного текста рыбы на русском языке</li>
                    <li>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более
                        менее осмысленного текста рыбы на русском языке</li>
                </ol>
                <div class="subtitle-ul-item-block">Пример пронумерованного списка:</div>
                <ol class="card-ol-item">
                    <li>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более
                        менее осмысленного текста рыбы на русском языке</li>
                    <li>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более
                        менее осмысленного текста рыбы на русском языке</li>
                    <li>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более
                        менее осмысленного текста рыбы на русском языке</li>
                    <li>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более
                        менее осмысленного текста рыбы на русском языке</li>
                    <li>Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько абзацев более
                        менее осмысленного текста рыбы на русском языке</li>
                </ol>
                <div class="blue-block-attention">
                    По своей сути рыбатекст является альтернативой традиционному lorem
                    ipsum, который вызывает у некторых людей недоумение прочитать рыбу
                    текст. В отличии от lorem ipsum, текст рыба на русском языке наполнит
                </div>
                <div class="statistic-block">
                    <div class="date-publick">Дата публикации: 08.12.2020</div>
                    <div class="social-publick">
                        <img src="/image/icon/shareplusoru.png" alt="">
                    </div>
                </div>
                <div class="next-news">
                    <div class="data-news">Следующая новость</div>
                    <div class="data-arrow">
                        <img src="/image/icon/black-arrow-right.png" alt="">
                    </div>
                </div>
            </div>
            <div class="column-right-pos">
                <div class="title-search-column">Поиск</div>
                <div class="form-column-busines">
                    <form>
                        <input type="text" placeholder="Введите текст" tabindex="1">
                        <span><img src="/image/icon/search.png" alt=""></span>
                    </form>
                </div>
                <div class="underline-column-busines">
                </div>
                <div class="title-search-item-column">Категории</div>
                <div class="type-category-column">
                    <div class="category-item ">
                        <div class="name-category active-category">Бизнес</div>
                        <div class="num-category active-category-num">3</div>
                    </div>
                    <div class="category-item">
                        <div class="name-category">Инвестиции</div>
                        <div class="num-category">2</div>
                    </div>
                    <div class="category-item">
                        <div class="name-category">Продажа</div>
                        <div class="num-category">5</div>
                    </div>
                    <div class="category-item">
                        <div class="name-category">Стратегия</div>
                        <div class="num-category">3</div>
                    </div>
                    <div class="category-item">
                        <div class="name-category">Аналитика</div>
                        <div class="num-category">8</div>
                    </div>
                    <div class="category-item">
                        <div class="name-category">Стартапы</div>
                        <div class="num-category">2</div>
                    </div>
                </div>
                <div class="underline-column-busines">
                </div>
                <div class="title-search-item-column">Популярные новости</div>
                <div class="type-news-column">
                    <div class="item-type-news-column-search">
                        <div class="pict-item-type-search">
                            <img src="/image/pictur/text-box-1.png" alt="">
                        </div>
                        <div class="text-box-search">
                            <div class="title-text-box">Мы использовали код речей</div>
                            <div class="date-text-box">Январь 2021</div>
                        </div>

                    </div>
                    <div class="item-type-news-column-search">
                        <div class="pict-item-type-search">
                            <img src="/image/pictur/text-box-2.png" alt="">
                        </div>
                        <div class="text-box-search">
                            <div class="title-text-box">Мы использовали код речей</div>
                            <div class="date-text-box">Январь 2021</div>
                        </div>
                    </div>
                    <div class="item-type-news-column-search">
                        <div class="pict-item-type-search">
                            <img src="/image/pictur/text-box-3.png" alt="">
                        </div>
                        <div class="text-box-search">
                            <div class="title-text-box">Мы использовали код речей</div>
                            <div class="date-text-box">Январь 2021</div>
                        </div>

                    </div>
                </div>
                <div class="underline-column-busines">
                </div>
                <div class="title-search-item-column">Теги</div>
                <div class="type-tag">
                    <div class="tag-item tag-active">Аналитика</div>
                    <div class="tag-item">Стартап</div>
                    <div class="tag-item">Бизнес</div>
                    <div class="tag-item">Стартап</div>
                    <div class="tag-item">Бизнес</div>
                    <div class="tag-item">Инвестиции</div>
                </div>
                <div class="underline-column-busines">
                </div>
                <div class="title-search-item-column">Мы в соцсетях</div>
                <div class="social-icon">
                    <div class="facebook">
                        <img src="/image/icon/facebook.png" alt="">
                    </div>
                    <div class="twitter">
                        <img src="/image/icon/twitter.png" alt="">
                    </div>
                    <div class="instagram">
                        <img src="/image/icon/instagram.png" alt="">
                    </div>
                </div>
                <div class="underline-column-busines">
                </div>
                <div class="banner-search">
                    <img src="/image/pictur/Banner.png" alt="">
                </div>
            </div>
        </div>
        <!-- <div class="pagination-block">
            <div class="item-pagionation">
                <ul>
                    <li class="active-pag">1</li>
                    <li>2</li>
                    <li>3</li>
                    <li>...</li>
                    <li><img src="/image/icon/black-arrow-right.png" alt=""></li>
                </ul>
            </div>
        </div> -->
        <div class="banner-join-investor">
            <div class="container">
                <div class="wrapper-banner-join-investor">
                    <div class="title-banner-join-investor">Присоединяйся к нашей профессиональной команде</div>
                    <div class="desc-banner-join-investor">ФРИЛАНС БИЗНЕС ПЛАТФОРМА ДЛЯ ИНВЕСТИЦИЙ</div>
                    <div class="btn-banner-join">
                        <button>Начать</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
