@extends('layouts.app')
@push('styles')
<link rel="stylesheet" href="{{ mix('css/app.css') }}">
@endpush
@section('content')
<div class="wrapper-main-page">
    <div class="main-preview">
        <div class="container">
            <div class="text-preview">
                <div class="title-frilans">ФРИЛАНС БИЗНЕС ПЛАТФОРМА ДЛЯ ИНВЕСТИЦИЙ</div>
                <div class="item-frilans">
                    <ul>
                        <li>Фриланс</li>
                        <li>Новости</li>
                        <li>Поиск и подбор инвестиций</li>
                    </ul>
                </div>
                <div class="btn-group-frilans">
                    <button class="investor">Найти инвестора</button>
                    <button class="employer">Найти работодателя</button>
                </div>
            </div>
        </div>
        <div class="scroll">
            <a href="/">
                <img src="/image/icon/Scroll.png" alt="">
            </a>
        </div>
    </div>
    <div class="fresh-news">
        <div class="container">
            <div class="swiper-container slide-news">
                <div class="swiper-wrapper">
                    <div class="swiper-slide">
                        <div class="wrapper-fresh-news">
                            <div class="text-news">
                                <div class="description-news">Фриланс новости</div>
                                <div class="title-news">Свежие новости от платформы</div>
                                <div class="subtitle-news">По своей сути рыбатекст является альтернативой традиционному
                                    lorem ipsum,
                                    который вызывает у некторых людей недоумение при попытках прочитать рыбу</div>
                                <div class="btn-news">
                                    <button>Все новости</button>
                                </div>
                            </div>
                            <div class="pictures-news">
                                <div class="first-news-image">
                                    <img src="/image/pictur/news-1.png" alt="">
                                    <!-- <div class="play-video">
												<img src="/image/icon/circle-play.png" alt="">
										  </div> -->
                                </div>
                                <div class="second-news-image">
                                    <img src="/image/pictur/news-2.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="wrapper-fresh-news">
                            <div class="text-news">
                                <div class="description-news">Фриланс новости</div>
                                <div class="title-news">Свежие новости от платформы</div>
                                <div class="subtitle-news">По своей сути рыбатекст является альтернативой традиционному
                                    lorem ipsum,
                                    который вызывает у некторых людей недоумение при попытках прочитать рыбу</div>
                                <div class="btn-news">
                                    <button>Все новости</button>
                                </div>
                            </div>
                            <div class="pictures-news">
                                <div class="first-news-image">
                                    <img src="/image/pictur/news-1.png" alt="">
                                    <!-- <div class="play-video">
												<img src="/image/icon/circle-play.png" alt="">
										  </div> -->
                                </div>
                                <div class="second-news-image">
                                    <img src="/image/pictur/news-2.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="swiper-slide">
                        <div class="wrapper-fresh-news">
                            <div class="text-news">
                                <div class="description-news">Фриланс новости</div>
                                <div class="title-news">Свежие новости от платформы</div>
                                <div class="subtitle-news">По своей сути рыбатекст является альтернативой традиционному
                                    lorem ipsum,
                                    который вызывает у некторых людей недоумение при попытках прочитать рыбу</div>
                                <div class="btn-news">
                                    <button>Все новости</button>
                                </div>
                            </div>
                            <div class="pictures-news">
                                <div class="first-news-image">
                                    <img src="/image/pictur/news-1.png" alt="">
                                    <!-- <div class="play-video">
												<img src="/image/icon/circle-play.png" alt="">
										  </div> -->
                                </div>
                                <div class="second-news-image">
                                    <img src="/image/pictur/news-2.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="swiper-button-prev btn-news-prev">
            <img src="/image/icon/black-arrow.png" alt="">
        </div>
        <div class="swiper-button-next btn-news-next">
            <img src="/image/icon/wite-arrow.png" alt="">
        </div>
    </div>
    <div class="rating-news">
        <div class="container">
            <div class="wrapper-column-news">
                <div class="column-news">
                    <div class="icon-column-news">
                        <img src="/image/icon/rocket.png" alt="">
                    </div>
                    <div class="number-column-news">80+</div>
                    <div class="title-column-news">Стартапов</div>
                    <div class="subtitle-column-news">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру
                        сгенерировать </div>
                </div>
                <div class="column-news">
                    <div class="icon-column-news">
                        <img src="/image/icon/linework.png" alt="">
                    </div>
                    <div class="number-column-news">10x</div>
                    <div class="title-column-news">Предложений продажи бизнеса</div>
                    <div class="subtitle-column-news">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру
                        сгенерировать </div>
                </div>
                <div class="column-news">
                    <div class="icon-column-news">
                        <img src="/image/icon/like.png" alt="">
                    </div>
                    <div class="number-column-news">1.2k</div>
                    <div class="title-column-news">Инвестиционных проектов</div>
                    <div class="subtitle-column-news">Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру
                        сгенерировать </div>
                </div>
            </div>
        </div>
    </div>
    <div class="investments">
        <div class="container">
            <div class="wrapper-investments">
                <div class="investments-pucturs">
                    <div class="first-investments-pict">
                        <img src="/image/pictur/person-1.png" alt="">
                    </div>
                    <div class="second-investments-pict">
                        <img src="/image/pictur/person-2.png" alt="">
                    </div>
                </div>
                <div class="investments-text">
                    <div class="investments-column">
                        <div class="prev-icon">
                            <img src="/image/icon/connect.png" alt="">
                        </div>
                        <div class="prev-text-column">
                            <div class="title-column-investments">Инвестируйте</div>
                            <div class="descripton-column-investments">Добавьте Ваш Инвестиционный Интерес и получайте
                                обращения от проектов</div>
                        </div>
                    </div>
                    <div class="investments-column">
                        <div class="prev-icon">
                            <img src="image/icon/statistic.png" alt="">
                        </div>
                        <div class="prev-text-column">
                            <div class="title-column-investments">Найти инвестиции</div>
                            <div class="descripton-column-investments">Помогаем начинающим и опытным предпринимателям
                                найти
                                финансирование</div>
                        </div>
                    </div>
                    <div class="investments-column">
                        <div class="prev-icon">
                            <img src="/image/icon/money.png" alt="">
                        </div>
                        <div class="prev-text-column">
                            <div class="title-column-investments">Продать бизнес</div>
                            <div class="descripton-column-investments">Представим Ваше предложение о продаже кругу
                                заинтересованных</div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="employer-block">
        <div class="container">
            <div class="wrapper-employer">
                <div class="title-employer">Для инвесторов и работодателей</div>
                <div class="description-employer">По своей сути рыбатекст является альтернативой традиционному lorem
                    ipsum, который вызывает у некторых людей недоумение при попытках прочитать рыбу</div>
                <div class="wrapper-employer-column">
                    <div class="employer-column">
                        <div class="icon-item-column-employer">
                            <img src="/image/icon/strategia.png" alt="">
                        </div>
                        <div class="title-item-column-employer">Стратегия</div>
                        <div class="description-item-column-employer">Начинающему оратору отточить навык публичных
                            выступлений в домашних условиях. </div>
                        <div class="arrow-wrapper">
                            <a href="/"> <img src="/image/icon/right-arrow-blue.png" alt=""></a>
                        </div>
                    </div>
                    <div class="employer-column">
                        <div class="icon-item-column-employer">
                            <img src="/image/icon/analitica.png" alt="">
                        </div>
                        <div class="title-item-column-employer">Аналитика</div>
                        <div class="description-item-column-employer">Сайт рыбатекст поможет дизайнеру, верстальщику,
                            вебмастеру сгенерировать несколько абзацев </div>
                        <div class="arrow-wrapper">
                            <a href="/"> <img src="/image/icon/right-arrow-blue.png" alt=""></a>
                        </div>
                    </div>
                    <div class="employer-column">
                        <div class="icon-item-column-employer">
                            <img src="/image/icon/expiriens.png" alt="">
                        </div>
                        <div class="title-item-column-employer">Опыт</div>
                        <div class="description-item-column-employer">Несколько абзацев более менее осмысленного текста
                            рыбы на русском языке, а начинающему </div>
                        <div class="arrow-wrapper">
                            <a href="/"> <img src="/image/icon/right-arrow-blue.png" alt=""></a>
                        </div>
                    </div>
                </div>
                <div class="btn-employer-group">
                    <button>Смотреть полностью</button>
                </div>
            </div>
        </div>
    </div>
    <div class="investor-startup">
        <div class="special-container">
            <div class="wrapper-investor-startup">
                <div class="wrapper-for-design">
                    <div class="investor-startup-block-desc">
                        <div class="description-investor-startup">Инвесторы</div>
                        <div class="title-investor-startup">Инвесторы для Вашего стартапа</div>
                    </div>
                    <div class="investor-startup-block-arrow-slide">
                        <div class="swiper-button-prev btn-investor-prev">
                            <img src="/image/icon/wite-arrow.png" alt="">
                        </div>
                        <div class="swiper-button-next btn-investor-next">
                            <img src="/image/icon/black-arrow.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="wrapper-slider-startup">
                    <div class="swiper-container slide-investor-startup ">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide slider-str">
                                <div class="startup-column">
                                    <div class="item-startup-prof">
                                        <div class="item-icon-startup">
                                            <img src="/image/pictur/investor-1.png" alt="">
                                        </div>
                                        <div class="item-name-sartup">Иванов Андрей Константинович</div>
                                    </div>
                                    <div class="description-startup-prof">
                                        По своей сути рыбатекст является альтернативой традиционному lorem ipsum,
                                        который вызывает у
                                        некторых людей недоумение при попытках прочитать рыбу текст.
                                    </div>
                                    <div class="arrow-wrapper-investor">
                                        <a href="/"> <img src="/image/icon/right-arrow-blue.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide slider-str">
                                <div class="startup-column">
                                    <div class="item-startup-prof">
                                        <div class="item-icon-startup">
                                            <img src="/image/pictur/investor-1.png" alt="">
                                        </div>
                                        <div class="item-name-sartup">Иванов Андрей Константинович</div>
                                    </div>
                                    <div class="description-startup-prof">
                                        По своей сути рыбатекст является альтернативой традиционному lorem ipsum,
                                        который вызывает у
                                        некторых людей недоумение при попытках прочитать рыбу текст.
                                    </div>
                                    <div class="arrow-wrapper-investor">
                                        <a href="/"> <img src="/image/icon/right-arrow-blue.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide slider-str">
                                <div class="startup-column">
                                    <div class="item-startup-prof">
                                        <div class="item-icon-startup">
                                            <img src="/image/pictur/investor-1.png" alt="">
                                        </div>
                                        <div class="item-name-sartup">Иванов Андрей Константинович</div>
                                    </div>
                                    <div class="description-startup-prof">
                                        По своей сути рыбатекст является альтернативой традиционному lorem ipsum,
                                        который вызывает у
                                        некторых людей недоумение при попытках прочитать рыбу текст.
                                    </div>
                                    <div class="arrow-wrapper-investor">
                                        <a href="/"> <img src="/image/icon/right-arrow-blue.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide slider-str">
                                <div class="startup-column">
                                    <div class="item-startup-prof">
                                        <div class="item-icon-startup">
                                            <img src="/image/pictur/investor-1.png" alt="">
                                        </div>
                                        <div class="item-name-sartup">Иванов Андрей Константинович</div>
                                    </div>
                                    <div class="description-startup-prof">
                                        По своей сути рыбатекст является альтернативой традиционному lorem ipsum,
                                        который вызывает у
                                        некторых людей недоумение при попытках прочитать рыбу текст.
                                    </div>
                                    <div class="arrow-wrapper-investor">
                                        <a href="/"> <img src="/image/icon/right-arrow-blue.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="business">
        <div class="container">
            <div class="wrapper-business">
                <div class="score-block">
                    <div class="title-score-block">Возможности для бизнеса</div>
                    <!-- <div class="ldBar" data-value="70" data-preset="energy"></div>
                    <div class="ldBar" data-value="90" data-preset="energy"></div>
                    <div class="ldBar" data-value="80" data-preset="energy"></div>
                    <div class="ldBar" data-value="60" data-preset="energy"></div> -->
                </div>
                <div class="pictures-business-block">
                    <div class="first-business-images">
                        <img src="/image/pictur/biznes-1.png" alt="">
                    </div>
                    <div class="second-business-images">
                        <img src="/image/pictur/biznes-2.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="question">
        <div class="question-wrapper">
            <div class="question-image">
                <img src="/image/pictur/investor-bg.png" alt="">
            </div>
            <div class="question-text">
                <div class="wrapper-text-question">
                    <div class="title-question-answer">Часто задаваемые вопросы</div>
                    <div class="items-block-question">
                        <div class="item-question">
                            <div class="name-question">
                                <span></span> Случайный вопрос на тему фриланса?
                            </div>
                            <div class="name-answer hide">
                                Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько
                                абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору
                                отточить навык публичных выступлений в домашних условиях.
                            </div>
                        </div>
                        <div class="item-question">
                            <div class="name-question action-question">
                                <span></span> Случайный вопрос на тему фриланса?
                            </div>
                            <div class="name-answer">
                                Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько
                                абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору
                                отточить навык публичных выступлений в домашних условиях.
                            </div>
                        </div>
                        <div class="item-question">
                            <div class="name-question">
                                <span></span> Случайный вопрос на тему фриланса?
                            </div>
                            <div class="name-answer hide">
                                Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько
                                абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору
                                отточить навык публичных выступлений в домашних условиях.
                            </div>
                        </div>
                        <div class="item-question">
                            <div class="name-question">
                                <span></span> Случайный вопрос на тему фриланса?
                            </div>
                            <div class="name-answer hide">
                                Сайт рыбатекст поможет дизайнеру, верстальщику, вебмастеру сгенерировать несколько
                                абзацев более менее осмысленного текста рыбы на русском языке, а начинающему оратору
                                отточить навык публичных выступлений в домашних условиях.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="startup">
        <div class="container">
            <div class="startup-wrapper">
                <div class="title-startup">Стартапы</div>
                <div class="subtitle-startup">В отличии от lorem ipsum, текст рыба на русском языке наполнит любой макет
                    непонятным смыслом и придаст неповторимый колорит.</div>
                <div class="block-invis">
                    <div class="clear-block"></div>
                    <div class="btn-toggle-startup">
                        <div class="news-toggle">Новые</div>
                        <label class="switch">
                            <input type="checkbox" class="toggle-check">
                            <div class="toggle-slider"></div>
                        </label>
                        <div class="popul-toggle">Популярные</div>
                    </div>
                    <div class="view-more">
                        <a href="/">Смотреть ещё</a>
                    </div>
                </div>

                <div class="wrapper-startup-column-group">
                    <div class="startups-column">
                        <div class="str-item-img">
                            <img src="/image/pictur/startup-1.png" alt="">
                        </div>
                        <div class="str-item-date">01.02.2021</div>
                        <div class="str-item-price">
                            <span>20% в год</span><span>$ 500.000 мин</span>
                        </div>
                        <div class="str-item-text">ТОО "Знак разворота" Безопасность дорожного движения автотранспортных
                            средств</div>
                        <div class="str-ite-btn">
                            <button>Контакты</button>
                        </div>
                        <div class="str-item-desc">Ищет инвестиции</div>
                    </div>
                    <div class="startups-column">
                        <div class="str-item-img">
                            <img src="/image/pictur/startup-1.png" alt="">
                        </div>
                        <div class="str-item-date">01.02.2021</div>
                        <div class="str-item-price">
                            <span>15% в год</span><span>$ 350.000 мин</span>
                        </div>
                        <div class="str-item-text">ТОО "Инвест" Продам инвестиционный контракт на сумму 12 000 000 евро
                        </div>
                        <div class="str-ite-btn">
                            <button>Контакты</button>
                        </div>
                        <div class="str-item-desc">Продажа</div>
                    </div>
                    <div class="startups-column">
                        <div class="str-item-img">
                            <img src="/image/pictur/startup-1.png" alt="">
                        </div>
                        <div class="str-item-date">01.02.2021</div>
                        <div class="str-item-price">
                            <span>20% в год</span><span>$ 500.000 мин</span>
                        </div>
                        <div class="str-item-text">МО-25 Строительство автомобильных и железнодорожных мостов.</div>
                        <div class="str-ite-btn">
                            <button>Контакты</button>
                        </div>
                        <div class="str-item-desc">Ищет инвестиции</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="banner-investor">
        <div class="container">
            <div class="banner-investor-wrapper">
                <div class="banner-title-investor">
                    <span>Инвестируйте!</span> В выдающиеся стартапы, находите инвестиции для себя
                </div>
                <div class="banner-subtitle-investor">
                    При создании генератора мы использовали небезизвестный универсальный код речей. Текст генерируется
                    абзацами случайным образом
                </div>
                <div class="banner-btn-investor">
                    <button>Начать</button>
                </div>
            </div>
        </div>
    </div>
    <div class="about-us">
        <div class="special-container">
            <div class="about-us-wrapper">
                <div class="title-about-us">
                    Говорят о нас
                </div>
                <div class="wrapper-slider-about-us">
                    <div class="swiper-container slider-about-us">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="column-about-us-main">
                                    <div class="title-column-about-main">Оставить свой отзыв о бизнес платформе</div>
                                    <div class="subtitle-column-about-main">При создании генератора мы использовали
                                        небезизвестный
                                        универсальный код речей.</div>
                                    <div class="about-us-arrow-slide">
                                        <a href="/"><img src="/image/icon/arrow-green.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="column-about-us">
                                    <div class="text-area-about">
                                        Всегда рекомендую платформу. Здесь реально помогут запустить ваш проект с
                                        минимальными
                                        вложениями, причем, как небольшой бизнес, так и нацеленный на завоевание
                                        мирового рынка.
                                        Денег хватит всем
                                    </div>
                                    <div class="present-block-about">
                                        <div class="picture-about-item">
                                            <img src="/image/pictur/talk-person-1.png" alt="">
                                        </div>
                                        <div class="text-about-item">
                                            <p>Даниил Тонкопий</p>
                                            <p>27 Декабря 2020</p>
                                        </div>
                                    </div>
                                    <div class="about-us-arrow-slide">
                                        <a href="/"><img src="/image/icon/arrow-grey.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="column-about-us">
                                    <div class="text-area-about">
                                        Всегда рекомендую платформу. Здесь реально помогут запустить ваш проект с
                                        минимальными
                                        вложениями, причем, как небольшой бизнес, так и нацеленный на завоевание
                                        мирового рынка.
                                        Денег хватит всем
                                    </div>
                                    <div class="present-block-about">
                                        <div class="picture-about-item">
                                            <img src="/image/pictur/talk-person-1.png" alt="">
                                        </div>
                                        <div class="text-about-item">
                                            <p>Даниил Тонкопий</p>
                                            <p>27 Декабря 2020</p>
                                        </div>
                                    </div>
                                    <div class="about-us-arrow-slide">
                                        <a href="/"><img src="/image/icon/arrow-grey.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="column-about-us">
                                    <div class="text-area-about">
                                        Всегда рекомендую платформу. Здесь реально помогут запустить ваш проект с
                                        минимальными
                                        вложениями, причем, как небольшой бизнес, так и нацеленный на завоевание
                                        мирового рынка.
                                        Денег хватит всем
                                    </div>
                                    <div class="present-block-about">
                                        <div class="picture-about-item">
                                            <img src="/image/pictur/talk-person-1.png" alt="">
                                        </div>
                                        <div class="text-about-item">
                                            <p>Даниил Тонкопий</p>
                                            <p>27 Декабря 2020</p>
                                        </div>
                                    </div>
                                    <div class="about-us-arrow-slide">
                                        <a href="/"><img src="/image/icon/arrow-grey.png" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-pagination"></div>
                        <!-- <div class="swiper-button-prev btn-about-us-prev"></div>
                        <div class="swiper-button-next btn-about-us-next"></div> -->
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div class="social-block">
        <div class="container">
            <div class="social-block-wrapper">
                <div class="text-social">
                    <div class="title-social">Мы в соцсетях</div>
                    <div class="description-social">В отличии от lorem ipsum, текст рыба на русском языке наполнит любой
                        макет непонятным смыслом и придаст неповторимый колорит советских времен.</div>
                    <div class="social-icon">
                        <div class="facebook">
                            <img src="/image/icon/facebook.png" alt="">
                        </div>
                        <div class="twitter">
                            <img src="/image/icon/twitter.png" alt="">
                        </div>
                        <div class="instagram">
                            <img src="/image/icon/instagram.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="form-social">
                    <div class="wrapper-form-soc">
                        <div class="title-form">Присоединиться</div>
                        <div class="subtitle-form">Авторизация/Регистрация</div>
                        <form>
                            <input class='inp-social' type="email" tabindex="1" placeholder="Ваш Email">
                            <div class="btn-group-form-social">
                                <button class="btn-join-soc">Войти</button>
                                <button class="btn-reg-soc">Регистрация</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
