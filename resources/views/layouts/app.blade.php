<!DOCTYPE html>
<html lang="ru">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{ mix('css/app.css') }}">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link
        href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <title>BHUB</title>
</head>

<body>
    <div id="header">
        <div class="container">
            <div class="wrapper-header">
                <a href="/">
                    <div class="logo">
                        <img src="/image/Logo.png" alt="">
                    </div>
                </a>
                <div class="nav-menu">
                    <div class="nav-item">
                        <ul>
                            <li>Главная</li>
                            <li>Бизнес новости</li>
                            <li>Стартапы</li>
                            <li>Фрилансеры</li>
                        </ul>
                    </div>
                </div>
                <div class="search">
                    <img src="/image/icon/search.png" alt="">
                </div>
                <div class="join-group">
                    <button>Войти</button>
                </div>

            </div>
        </div>
    </div>
    @yield('content')
    <div id="footer">
        <div class="container">
            <div class="footer-wrapper">
                <div class="block-footer">
                    <div class="column-footer-first">
                        <div class="logo">
                            <img src="/image/logo-footer.png" alt="">
                        </div>
                        <div class="description-footer">В отличии от lorem ipsum, текст рыба на русском языке наполнит
                        </div>
                        <div class="social-icon">
                            <div class="facebook">
                                <img src="/image/icon/facebook.png" alt="">
                            </div>
                            <div class="twitter">
                                <img src="/image/icon/twitter.png" alt="">
                            </div>
                            <div class="instagram">
                                <img src="/image/icon/instagram.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="column-footer">
                        <div class="name-column-footer">Инвестору</div>
                        <ul class="submane-ul">
                            <li>Стартапы</li>
                            <li>Инвестпроекты</li>
                            <li>Кредиты Бизнесу</li>
                            <li>Онлайн Кредит</li>
                        </ul>
                    </div>
                    <div class="column-footer">
                        <div class="name-column-footer">Бизнесмену</div>
                        <ul class="submane-ul">
                            <li>Получить Займ</li>
                            <li>Продать Бизнес</li>
                            <li>Программу Запросы</li>
                            <li>Запросы Инвесторов</li>
                        </ul>
                    </div>
                    <div class="column-footer">
                        <div class="name-column-footer">Профессионалу</div>
                        <ul class="submane-ul">
                            <li>Создать Программу</li>
                            <li>Продать Услугу</li>
                        </ul>
                    </div>
                    <div class="column-footer">
                        <div class="name-column-footer">Контакты</div>
                        <div class="adress-footer">
                            Казахстан, Алматы, Пушкина, д.1
                        </div>
                        <div class="tel-footer">
                            <span>+7 (987) 65-43-21</span>
                            <span>+7 (987) 65-43-21</span>
                        </div>
                        <div class="mail-footer">Info@bhub.mytest.kz</div>
                    </div>

                </div>
                <div class="underline"></div>
                <div class="permisson">
                    <p>©2021 Все права защищены</p>
                    <p>Политика конфиденциальности</p>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ mix('js/app.js') }}"></script>
</body>

</html>
