import Swiper from 'swiper/bundle';

const sliderSwiper = document.querySelector('.slide-news' );
let mySwiper = new Swiper(sliderSwiper, {
	loop: true,

	navigation: {
		nextEl: '.btn-news-next',
		prevEl: '.btn-news-prev',
	 },
});


const sliderStartup = document.querySelector('.slide-investor-startup' );
let my = new Swiper(sliderStartup, {
	loop: true,
	slidesPerView: 3.7,
	spaceBetween: 20,
	watchOverflow: true,
	// centeredSlides: true,

	navigation: {
		nextEl: '.btn-investor-next',
		prevEl: '.btn-investor-prev',
	 },
});




const sliderAboutUs = document.querySelector('.slider-about-us');
let myy = new Swiper(sliderAboutUs, {
	loop: true,
	slidesPerView: 3.7,
	spaceBetween: 20,
	watchOverflow: true,
	// centeredSlides: true,

	navigation: {
		nextEl: '.btn-investor-next',
		prevEl: '.btn-investor-prev',
	 },
});