<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('index');
});

Route::get('/businesNews', function () {
	return view('businesNews.index');
});

Route::get('/startUp', function () {
	return view('startUp.index');
});

Route::get('/businesPage', function () {
	return view('businesPage.index');
});

Route::get('/commerch', function () {
	return view('commerch.index');
});

Route::get('/frilanser', function () {
	return view('frilanser.index');
});

Route::get('/cardItem', function () {
	return view('cardItem.index');
});

Route::get('/cabinet', function () {
	return view('cabinet.index');
});

